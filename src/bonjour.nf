/* test process */
process write_bonjour_cava {
  /* publishDir "results/tests/", mode: 'copy' */
  
  output:
file "message.txt" into parole

  script:
"""
echo bonjour > message.txt
echo ca va >> message.txt
"""
}

/* another process */
process last_line {
  publishDir "results/tests/", mode: 'copy'
  
  input:
val STR from 'voici', 'voila'

  output:
file OUTSTR into line

  script:
template 'bonjour.sh' > $OUTSTR

}


/* essayer ceci:
*
*  script:
*  """
*  Sweave -nd -lc src/test.Rnw 
*  """
*
* puis avec des fichiers i/o
*/



